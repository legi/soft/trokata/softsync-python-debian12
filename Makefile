
sync:
	pdm sync --clean

lock:
	pdm lock

test:
	pdm run nox -s test --no-venv
