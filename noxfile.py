import os

import nox

os.environ.update({"PDM_IGNORE_SAVED_PYTHON": "1"})
nox.options.reuse_existing_virtualenvs = True


@nox.session
def test(session):
    """Test the virtual environment"""
    session.run_install("pdm", "sync", "--clean", external=True)

    def _run(command, env=None):
        session.run(*command.split(), external=False, env=env)

    _run("pytest tests")
    _run("pytest --pyargs fluidimage", env={"OMP_NUM_THREADS": "1"})
    _run("pytest --pyargs fluidsim")
    _run(
        "mpirun -np 2 pytest --pyargs fluidsim",
        env={"OMPI_ALLOW_RUN_AS_ROOT": "1", "OMPI_ALLOW_RUN_AS_ROOT_CONFIRM": "1"},
    )


@nox.session
def book(session):
    session.install("jupyter-book")

    session.run("jupyter-book", "build", "book", external=False)
