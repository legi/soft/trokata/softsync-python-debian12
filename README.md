# Source to create a Python module on Debian 12

The goal of this repository is to create in the CI a tar archive of a virtual
environment `/opt/python/3.11.2`. This environment can be activated at LEGI
with `module load python/3.11.2`.

The latest version can be downloaded at this address:
<https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/27252/jobs/artifacts/main/download?job=module:build>

## A problem with the module at LEGI?

Please feel an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/softsync-python-debian12/-/issues).

## Documentation about Python usage at LEGI and this module

See https://fluidhowto.readthedocs.io/en/latest/python.html

## Authors

Main authors:

* [Pierre Augier](mailto:pierre.augier[A]univ-grenoble-alpes.fr), LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France

Contributors:

* Gabriel Moreau, UMR 5519 / CNRS UGA G-INP, Grenoble, France
* Cyrille Bonamy, UMR 5519 / CNRS UGA G-INP, Grenoble, France

## Copyright

Copyright (C) 2024, LEGI UMR 5519 / CNRS UGA G-INP, Grenoble, France

Licence: [MIT](./LICENSE.md)
