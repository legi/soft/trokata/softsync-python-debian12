# How to contribute to the LEGI Python module and the website?

## How to update the module?

1. Clone this repo locally on a Debian 12 (Bookworm).

2. Install PDM and Nox for example as explained in
   [the documentation](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/softsync-python-debian12/setup-apps.html).

3. Check that it works locally: create the virtualenv (`make`) and test (`make test`).

4. Modify the dependencies listed in the pyproject.toml file and update the lockfile
   (`make lock`).

5. Test if the module still works (`make test`).

6. Open a merge request.

Note: to modify the Docker image used for the CI, create a new branch, modify
`docker/Dockerfile` and comment the rules for the `image:build`. Re-comment the rules
when the new Docker image has been created (before merging).

## How to update the documentation?

1. Clone this repo locally on a Debian 12 (Bookworm).

2. Install Nox for example as explained in
   [the documentation](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/softsync-python-debian12/setup-apps.html).

3. Build the doc locally with `nox -s book`.

4. Modify the source files in the directory `book`. Check that it still compiles locally
   (`nox -s book`).

5. Create a merge request.
