import os

import pytest
import matplotlib.pyplot as plt


def test_subplots():
    fig, ax = plt.subplots()


@pytest.mark.skipif("GITLAB_CI" in os.environ, reason="Crashes on gricad.gitlab CI")
def test_compat_qt():
    from matplotlib.backends.qt_compat import QtWidgets

    try:
        app = QtWidgets.QApplication([])
    except RuntimeError:
        app = QtWidgets.QApplication.instance()
